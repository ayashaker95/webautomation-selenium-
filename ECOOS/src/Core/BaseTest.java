package Core;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeSuite;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;

public class BaseTest {
public WebDriver driver;
 

  @BeforeSuite
  public void beforeSuite() {
	  String exePath = "C:\\Users\\Ashaker\\eclipse-workspace\\ECOOS\\chromedriver.exe";
	  System.setProperty("webdriver.chrome.driver", exePath);
	   driver = new ChromeDriver();
	  driver.get("http://172.22.1.107/login");  
  }

  @AfterSuite
  public void afterSuite() {
	  driver.quit();
  }
 
}
