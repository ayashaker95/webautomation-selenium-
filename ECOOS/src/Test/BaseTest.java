package Test;

import org.testng.annotations.Test;
import java.io.IOException;
import org.testng.annotations.DataProvider;
import static Core.ReadWriteExcelFile.getExcelDataWithoutHeader;
import Core.BaseTest;
import Pages.LoginPage;

public class Test1 extends BaseTest {
  @Test(dataProvider="ValidDataProvider1")
public void logintest(String UserName, String Password) throws InterruptedException {
	  
	  System.out.println("Test ----------");
	  LoginPage login = new LoginPage(driver);
	  login.loginToSite(UserName,Password);
	  login.method2();
  }
  
  @DataProvider
  public Object[][] ValidDataProvider1() throws IOException {

                  return getExcelDataWithoutHeader("TestData1.xls", "Sheet1");
  }

}




